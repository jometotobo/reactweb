import React, { useState } from "react";
import Home from '../components/Home'
import '../styles/main.css'
import { lista } from '../assets/lista'
//import Fuse from "fuse.js";

/*function App() {
  const [data, setData] = useState(lista);
  const searchData = (query) => {
    if (!query) {
      console.warn(query)  
      setData(lista);
      return;
    }
    const myChangeHandler = (event) => {
        alert(event.target.value)
    }    
    //const result = fuse.search(query);
    /*const finalResult = [];
    if (result.length) {
      result.forEach((item) => {
        finalResult.push(item.item);
      });
      setData(finalResult);
    } else {
      setData([]);
    }*/
  //};
  /*return (
    <div>
      <h1 className="Title">Datos</h1>      
      <input type="text" id="name" name="name" minlength="4" maxlength="8" size="10" onChange={this.myChangeHandler}/>      
      <div className="contenedor">
            <Home title={"Casas en Bolivia"} data={lista} />
      </div>
    </div>    
  );
}*/
class App extends React.Component {
    constructor(props){
        super(props);  
        this.state={
            data:lista            
        }
        this.onInputchange = this.onInputchange.bind(this);
        this.onSubmitForm = this.onSubmitForm.bind(this);
    }
    onInputchange(event) {
        this.setState({
          [event.target.name]: event.target.value
        });
    }
    onSubmitForm() {                                    
        const result = lista.find(element => element.beds === parseInt(this.state.name));
        console.warn(resul)           
        if (result !== undefined) {
        this.setState({data:[result]})         
        } else { 
        alert('No existe registro')       
        this.setState({data:lista})
        }        
    }
    componentDidMount(){
        this.setState({data:lista})        
    }                          
    render() {
      return (
        <div>
      <h1 className="Title">Datos</h1>      
      <input type="text" id="name" name="name" minlength="4" maxlength="8" size="10" value={this.state.name} onChange={this.onInputchange}/>
      <button onClick={this.onSubmitForm}>BUSCAR</button>      
      <div className="contenedor">
            <Home title={"Casas en Bolivia"} data={this.state.data} />
      </div>      
    </div>          
      )
    }
  }
export default App;